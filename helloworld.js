var myApp = angular.module('helloworld', ['ui.router']);

myApp.config(function($stateProvider) {
  var helloState = {
    name: 'hello',
    url: '/hello',
    template: '<h3>hello world!</h3>'
  }

  var aboutState = {
    name: 'about',
    url: '/about',
    templateUrl: 'info.html'
  }

  $stateProvider.state(helloState);
  $stateProvider.state(aboutState);
});
